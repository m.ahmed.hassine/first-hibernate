package fr.afcepf.ai107.hibernate.program;

import org.hibernate.Session;

import fr.afcepf.ai107.hibernate.util.HibernateUtil;

public class TestSessionHibernate {

	public static void main(String[] args) {
		Session session = null;
		
		session = HibernateUtil.getSession();
		System.out.println("Session Hibernate OK");
		
		session.close();
		System.exit(0);

	}

}
