package fr.afcepf.ai107.hibernate.program;

import java.util.Date;
import java.util.HashSet;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afcepf.ai107.hibernate.entity.Cat;
import fr.afcepf.ai107.hibernate.entity.User;
import fr.afcepf.ai107.hibernate.util.HibernateUtil;

public class TestRelationnel {

	public static void main(String[] args) {
		Session session = null;
		Transaction tx = null;
		
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			User user = new User(null, "Toto", "de Toto", "toto", "toto", new HashSet<Cat>());
			session.save(user);
			
			Cat cat1 = new Cat(null, "Garfield", "Chat roux", new Date(), "garfield.jpg", user);
			Cat cat2 = new Cat(null, "Félix", "Chat noir", new Date(), "felix.jpg", user);
			Cat cat3 = new Cat(null, "Grosminet", "Chat vicieux", new Date(), "grosminet.jpg", user);
			
			//Premier test relationnel
//			session.save(cat1);
//			session.save(cat2);
//			session.save(cat3);
			
			//Deuxième test relationnel
			user.getCats().add(cat1);
			user.getCats().add(cat2);
			user.getCats().add(cat3);
			
			
			tx.commit();
		}catch(Exception e) {
			e.printStackTrace();
			tx.rollback();
		}finally {
			session.close();
		}
		System.exit(0);
	}

}
