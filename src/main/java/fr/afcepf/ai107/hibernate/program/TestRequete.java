package fr.afcepf.ai107.hibernate.program;

import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import fr.afcepf.ai107.hibernate.entity.Cat;
import fr.afcepf.ai107.hibernate.entity.User;
import fr.afcepf.ai107.hibernate.util.HibernateUtil;

public class TestRequete {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Session session = null;
		Transaction tx = null;
		
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			//Requête SQL Native en Hibernate
			Query querySQL = session.createSQLQuery("SeLect * fRoM uSeR");
			List<Object[]> result = querySQL.list();
			for (Object[] objects : result) {
				System.out.println(objects[2] + " " + objects[4]);
			}
			
			//La même chose avec une requête HQL
			Query queryHQL = session.createQuery("SELECT u FROM User u");
			List<User> users = queryHQL.list();
			for (User user : users) {
				System.out.println("Avec HQL: " + user.getName() + " " + user.getSurname());
			}
			
			//Requête HQL avec Paramètre
			Query queryHQLParam = session.createQuery("SELECT c.name FROM Cat c WHERE c.name LIKE :paramPartialName");
			queryHQLParam.setParameter("paramPartialName", "%e%");
			List<String> names = queryHQLParam.list();
			for (String name : names) {
				System.out.println("Resultat HQLParam " + name);
			}
			
			//Différents moyens pour récupérer tous les chats de Tata
			
			//Méthode 1: la plus lazy
			
			User tata = (User) session.get(User.class, 2);
			Set<Cat> tatasCat = tata.getCats();
			for (Cat cat : tatasCat) {
				System.out.println("lazyMethod " + cat.getName());
			}
			
			//Methode 2: HQL version 1
			
			Query queryHQLTata1 = session.createQuery("SELECT c FROM Cat c WHERE c.user.id = 2");
			List<Cat> tatasCat2 =  queryHQLTata1.list();
			for (Cat cat : tatasCat2) {
				System.out.println("HQLV1 " + cat.getName());
			}
			
			//Methode 3 : HQL version 2
			Query queryHQLTata2 = session.createQuery("SELECT u.cats FROM User u WHERE u.id = 2");
			List<Cat> tatasCat3 = queryHQLTata2.list();
			for (Cat cat : tatasCat3) {
				System.out.println("HQLV2 " + cat.getName());
			}
			
			Criteria crit = session.createCriteria(User.class);
			crit.add(Restrictions.like("surname", "de", MatchMode.ANYWHERE));
			crit.addOrder(Order.desc("id"));
			List<User> usersCrit = crit.list();
			for (User user : usersCrit) {
				System.out.println("Criteria " + user.getSurname());
			}
			
			tx.commit();
		}catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}finally {
			session.close();
		}

	}

}
