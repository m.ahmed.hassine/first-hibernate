package fr.afcepf.ai107.hibernate.program;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afcepf.ai107.hibernate.entity.User;
import fr.afcepf.ai107.hibernate.util.HibernateUtil;

public class TestCRUD {

	public static void main(String[] args) {
		
		Session session = null;
		Transaction tx = null;
		
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			
			//Création d'un user
			User user = new User(null, "Hib", "ernate", "hib", "ernate", null);
			session.save(user);
			System.out.println("Utilisateur n° " + user.getId() + " bien créé");
			
			//Récupérer un enregistrement à partir de son ID
			User userbis = (User) session.get(User.class, 1) ;
			System.out.println(userbis.getName() + userbis.getSurname());
			
			//Update une entrée dans la base
			userbis.setName("toto");
			
			//Suppression
//			session.delete(userbis);
			
			tx.commit();
			
		}catch(Exception e) {
			tx.rollback();
		}finally {
			session.close();
		}
		System.exit(0);
	}

}
