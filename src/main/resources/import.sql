INSERT INTO `user` (name, surname, login, password) VALUES ('Toto', 'de Toto', 'toto', 'toto');
INSERT INTO `user` (name, surname, login, password) VALUES ('Tata', 'de Tata', 'tata', 'tata');
INSERT INTO `cat` (name, race, birth, photo, user_id) VALUES ('Ernest', 'Main Coon', '2012-12-12', 'ernest.jpg', 1);
INSERT INTO `cat` (name, race, birth, photo, user_id) VALUES ('Garfield', 'Chat roux', '2012-12-12', 'garfield.jpg', 1);
INSERT INTO `cat` (name, race, birth, photo, user_id) VALUES ('Zoubir', 'Persan', '2012-12-12', 'zoubir.jpg', 2);
INSERT INTO `cat` (name, race, birth, photo, user_id) VALUES ('Flipette', 'Chat flipé', '2012-12-12', 'flipette.jpg', 2);